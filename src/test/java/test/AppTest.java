package test;

import static org.junit.Assert.assertTrue;

import model.Monomial;
import model.Polynomial;
import model.additionalClasses.InputException;
import org.junit.Assert;
import org.junit.Test;

public class AppTest {
    public static Polynomial p1 = new Polynomial();
    public static Polynomial p2 = new Polynomial();

    public AppTest() {

    }

    @Test
    public void testAdd() throws InputException {
        p1.add(new Polynomial("2x^3+3"));
        p1.add(new Polynomial("3x"));
        p2.add(new Polynomial("2x^3"));
        p2.add(new Polynomial("3x"));
        p2.add(new Polynomial("3"));

        Assert.assertEquals(p1, p2);
    }

    @Test
    public void testSubstract() throws InputException {
        p1.add(new Polynomial("2x^3+6x"));
        p1.subtract(new Polynomial("3x"));
        p2.add(new Polynomial("2x^3"));
        p2.add(new Polynomial("3x"));

        Assert.assertEquals(p1, p2);
    }

    @Test
    public void testMultiply() throws InputException {
        p1.add(new Polynomial("2x^3+6x"));
        p1.multiply(new Polynomial("3"));
        p2.add(new Polynomial("6x^3+18x"));

        Assert.assertEquals(p1, p2);
    }

    @Test
    public void testDerivate() throws InputException {
        p1.add(new Polynomial("2x^3+6x"));
        p2.add(new Polynomial("2x^2"));

        Polynomial result = p1.derivate();

        Assert.assertEquals(p1, p2);
    }

    @Test
    public void testIntegrate() throws InputException {
        p1.add(new Polynomial("2x^3"));
        p2.add(new Polynomial("x^4/2"));

        Polynomial result = p1.integrate();

        Assert.assertEquals(p1, p2);
    }

}

