import controller.Controller;

import view.UI;


public class Main {

        public static void main(String[] args) {
            UI ui = new UI();

            Controller c = new Controller(ui);
        }
}
