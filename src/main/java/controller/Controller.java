package controller;

import model.additionalClasses.InputException;
import view.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import model.*;

public class Controller {

    private Data dataPanel;
    private Result resultPanel;
    private Polynomial p1;
    private Polynomial result;
    private char button;

    public Controller(UI view) {
        this.dataPanel = view.getData();
        this.resultPanel = view.getResults();
        ActionListener addition = new AddButtonListener();
        ActionListener subtraction = new SubtractButtonListener();
        ActionListener multiplication = new MultiplyButtonListener();
        ActionListener integration = new IntegrationButtonListener();
        ActionListener derivation = new DerivationButtonListener();
        ActionListener equal = new EqualsButtonListener();
        ActionListener delete = new DeleteButtonListener();

        dataPanel.setListeners(addition, subtraction, multiplication, integration, derivation, equal, delete);
    }

    private boolean checkResult(Polynomial poly) {
        if (poly.getElements().size() == 0) {
            return false;
        }

        return true;
    }

    class AddButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                addButtonPressed();
            } catch (InputException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage(), "Wrong Input (+)", JOptionPane.ERROR_MESSAGE);
                dataPanel.setFirstPolynomial("");
            }
            button = '+';
        }
    }

    private void addButtonPressed() throws InputException {

        if (!dataPanel.getFirstPolynomial().equals("")) {
            p1 = new Polynomial(dataPanel.getFirstPolynomial());
            result = new Polynomial(resultPanel.getResult());
            Polynomial r = p1.add(result);

            if (checkResult(r)) {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult(r.toString());
            } else {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult("0");
            }
        } else {
            JOptionPane.showMessageDialog(null, "The textbox is empty !", "Input Error (+)", JOptionPane.ERROR_MESSAGE);
        }
    }

    class SubtractButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                subtractButtonPressed();
            } catch (InputException exception1) {
                JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (-)", JOptionPane.ERROR_MESSAGE);
                dataPanel.setFirstPolynomial("");
            }
            button = '-';

        }
    }

    private void subtractButtonPressed() throws InputException {

        if (!dataPanel.getFirstPolynomial().equals("")) {
            p1 = new Polynomial(dataPanel.getFirstPolynomial());
            result = new Polynomial(resultPanel.getResult());

            if (resultPanel.getResult().equals("0") ) {
                resultPanel.setResult(p1.toString());
                dataPanel.setFirstPolynomial("");
            } else {
                Polynomial r = result.subtract(p1);

                if (checkResult(r)) {
                    dataPanel.setFirstPolynomial("");
                    resultPanel.setResult(r.toString());
                } else {
                    dataPanel.setFirstPolynomial("");
                    resultPanel.setResult("0");
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "The textbox is empty !", "Input Error (-)", JOptionPane.ERROR_MESSAGE);
        }

    }

    class MultiplyButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                multiplyButtonPressed();
            } catch (InputException exception1) {
                JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (*)", JOptionPane.ERROR_MESSAGE);
                dataPanel.setFirstPolynomial("");
            }
            button = '*';

        }
    }

    private void multiplyButtonPressed() throws InputException {

        if (!dataPanel.getFirstPolynomial().equals("")) {
            p1 = new Polynomial(dataPanel.getFirstPolynomial());

            if (resultPanel.getResult().equals("0")) {
                result = new Polynomial("1");
            } else {
                result = new Polynomial(resultPanel.getResult());
            }

            Polynomial r = result.multiply(p1);

            if (checkResult(r)) {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult(r.toString());
            } else {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult("0");
            }
        } else {
            JOptionPane.showMessageDialog(null, "The textbox is empty !", "Input Error (*)", JOptionPane.ERROR_MESSAGE);
        }
    }

    class IntegrationButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                integrationButtonPressed();
            } catch (InputException exception1) {
                JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (S)", JOptionPane.ERROR_MESSAGE);
                dataPanel.setFirstPolynomial("");
            }
            button = 'S';

        }
    }

    private void integrationButtonPressed() throws InputException {
        if (!dataPanel.getFirstPolynomial().equals("")) {
            p1 = new Polynomial(dataPanel.getFirstPolynomial());
            result = p1.integrate();
            if (checkResult(result)) {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult(result.toString());
            } else {
                resultPanel.setResult("0");
            }
        } else if (!resultPanel.getResult().equals("")) {

            result = result.integrate();
            resultPanel.setResult(result.toString());
        } else {
            JOptionPane.showMessageDialog(null, "The textbox is empty !", "Input Error(S)", JOptionPane.ERROR_MESSAGE);
        }
    }

    class DerivationButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                derivationButtonPressed();
            } catch (InputException exception1) {
                JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input(')", JOptionPane.ERROR_MESSAGE);
                dataPanel.setFirstPolynomial("");
            }
            button = '\'';

        }
    }

    private void derivationButtonPressed() throws InputException {
        if (!dataPanel.getFirstPolynomial().equals("")) {
            p1 = new Polynomial(dataPanel.getFirstPolynomial());
            result = p1.derivate();
            if (checkResult(result)) {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult(result.toString());
            } else {
                dataPanel.setFirstPolynomial("");
                resultPanel.setResult("0");
            }
        } else if (!resultPanel.getResult().equals("")) {
            result = result.derivate();
            resultPanel.setResult(result.toString());
        } else {
            JOptionPane.showMessageDialog(null, "The textbox is empty !", "Input Error(')", JOptionPane.ERROR_MESSAGE);
        }
    }

    class EqualsButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            switch (button) {
                case '+': {
                    try {
                        addButtonPressed();
                    } catch (InputException exception1) {
                        JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (+)", JOptionPane.ERROR_MESSAGE);
                        dataPanel.setFirstPolynomial("");
                    }
                    break;
                }

                case '-': {
                    try {
                        subtractButtonPressed();
                    } catch (InputException exception1) {
                        JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (-)", JOptionPane.ERROR_MESSAGE);
                        dataPanel.setFirstPolynomial("");
                    }
                    break;
                }

                case '*': {
                    try {
                        multiplyButtonPressed();
                    } catch (InputException exception1) {
                        JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input(*)", JOptionPane.ERROR_MESSAGE);
                        dataPanel.setFirstPolynomial("");
                    }
                    break;
                }

                case 'S': {
                    try {
                        integrationButtonPressed();
                    } catch (InputException exception1) {
                        JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (S)", JOptionPane.ERROR_MESSAGE);
                        dataPanel.setFirstPolynomial("");
                    }
                    break;
                }

                case '\'': {
                    try {
                        derivationButtonPressed();
                    } catch (InputException exception1) {
                        JOptionPane.showMessageDialog(null, exception1.getMessage(), "Wrong Input (/)", JOptionPane.ERROR_MESSAGE);
                        dataPanel.setFirstPolynomial("");
                    }
                    break;
                }

                default:
                    break;
            }
        }
    }

    class DeleteButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            dataPanel.setFirstPolynomial("");
            resultPanel.setResult("0");
        }
    }

}

