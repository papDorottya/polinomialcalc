package view;

import javax.swing.*;

public class UI extends JFrame {

    private Data data = new Data();
    private Result results = new Result();

    public UI() {
        setVisible(true);
        setTitle("Calculator");
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        add(results);
        add(data);

        pack();
    }

    public Result getResults() {
        return this.results;
    }
    public Data getData() {
        return this.data;
    }

}
