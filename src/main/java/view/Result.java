package view;

import java.awt.*;
import javax.swing.*;

public class Result extends JPanel {

    private JLabel result;
    private JLabel resultL;

    Color color2 = new Color(0xdcdcdc);
    Color color = new Color(0xFEFFFB);

    public Result() {
        this.setVisible(true);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(550, 65));
        this.setBackground(color);

        this.resultL = new JLabel("RESULT");
        this.resultL.setBounds(5, 10, 555, 30);
        this.resultL.setHorizontalAlignment(SwingConstants.CENTER);
        this.resultL.setOpaque(true);
        this.resultL.setBackground(color2);
        this.add(resultL);

        this.result = new JLabel("0");
        this.result.setBounds(5, 40, 555, 30);
        this.result.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(result);

    }

    public String getResult() {
        return result.getText();
    }

    public void setResult(String results) {
        this.result.setText(results);
    }

}
