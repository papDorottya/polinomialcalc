package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class Data extends JPanel {

    private JLabel textLabel;
    private JTextField textField;

    private JButton addButton;
    private JButton subButton;
    private JButton mulButton;
    private JButton derButton;
    private JButton intButton;
    private JButton equalsButton;
    private JButton deleteButton;

    Color color2 = new Color(0xdcdcdc);
    Color color = new Color(0xFEFFFB);

    public Data() {

        this.setVisible(true);
        this.setLayout(null);
        this.setBackground(color);
        this.setPreferredSize(new Dimension(567, 140));

        this.textField = new JTextField("");
        this.textField.setBounds(5, 10, 558, 30);
        this.textField.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(textField);

        this.addButton = new JButton("+");
        this.addButton.setBounds(5, 50, 107, 35);
        this.addButton.setActionCommand("addition");
        this.add(addButton);

        this.subButton = new JButton("-");
        this.subButton.setBounds(117, 50, 107, 35);
        this.subButton.setActionCommand("subtraction");
        this.add(subButton);

        this.mulButton = new JButton("*");
        this.mulButton.setBounds(229, 50, 107, 35);
        this.mulButton.setActionCommand("multiplication");
        this.add(mulButton);

        this.derButton = new JButton("'");
        this.derButton.setBounds(341, 50, 107, 35);
        this.derButton.setActionCommand("derivation");
        this.add(derButton);

        this.intButton = new JButton("S");
        this.intButton.setBounds(453, 50, 107, 35);
        this.intButton.setActionCommand("integration");
        this.add(intButton);

        this.equalsButton = new JButton("=");
        this.equalsButton.setBounds(5, 100, 450, 35);
        this.add(equalsButton);

        this.deleteButton = new JButton("DELETE");
        this.deleteButton.setBounds(460, 100, 100, 35);
        this.add(deleteButton);

    }

    public void setListeners(ActionListener addition, ActionListener subtraction, ActionListener multiplication, ActionListener integration, ActionListener derivation, ActionListener equal, ActionListener delete) {
        addButton.addActionListener(addition);
        subButton.addActionListener(subtraction);
        mulButton.addActionListener(multiplication);
        intButton.addActionListener(integration);
        derButton.addActionListener(derivation);
        equalsButton.addActionListener(equal);
        deleteButton.addActionListener(delete);
    }

    public String getFirstPolynomial() {
        return textField.getText();
    }

    public void setFirstPolynomial(String s) {
        this.textField.setText(s);
    }
}