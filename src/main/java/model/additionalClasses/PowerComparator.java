package model.additionalClasses;

import model.Monomial;

import java.util.Comparator;

public class PowerComparator implements Comparator<Monomial> {
    public int compare(Monomial obj1, Monomial obj2) {
        return obj2.getPower() - obj1.getPower();
    }
}
