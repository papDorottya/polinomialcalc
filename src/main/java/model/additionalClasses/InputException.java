package model.additionalClasses;

public class InputException extends Throwable {

    public InputException() {
        super("WRONG INPUT");
    }
}
