package model;

import model.additionalClasses.InputException;
import model.additionalClasses.PowerComparator;

import java.util.ArrayList;

public class Polynomial implements Cloneable{

    private ArrayList<Monomial> terms;

    public Polynomial() {
        this.terms = new ArrayList<Monomial>();
    }

    public Polynomial(String polinom) throws InputException {
        this.terms = obtainMonomials(polinom);
    }

    private void organize() {
        PowerComparator poli = new PowerComparator();
        this.terms.sort(poli);
    }

    public void cleaningThePoli() {
        ArrayList<Monomial> auxiliarTermsList = this.terms;
        int i = 0;
        while (i < auxiliarTermsList.size()) {
            if (auxiliarTermsList.get(i).getCoefficient() != 0) {
                i++;
            } else {
                auxiliarTermsList.remove(i);
            }
        }

        this.terms = auxiliarTermsList;
    }

    public Polynomial add(Polynomial polinom) {
        this.organize();
        polinom.organize();

        Polynomial result = new Polynomial();
        ArrayList<Monomial> termList = new ArrayList<>();

        int numberOfElements = Math.max(this.terms.get(0).getPower(), polinom.terms.get(0).getPower()) + 1;

        for(int i = 0; i < numberOfElements; i++) {
            termList.add(i, new Monomial(0, numberOfElements - 1 - i));
        }

        for (Monomial monom : polinom.terms) {
            int index = numberOfElements - 1 - monom.getPower();
            termList.set(index, termList.get(index).additionOp(monom));
        }

        for (Monomial monom : this.terms) {
            int index = numberOfElements - 1 - monom.getPower();
            termList.set(index, termList.get(index).additionOp(monom));
        }
        result.terms = termList;
        result.cleaningThePoli();

        return result;
    }

    public Polynomial subtract(Polynomial polinom) {
        this.organize();
        polinom.organize();

        Polynomial result = new Polynomial();
        ArrayList<Monomial> termList = new ArrayList<Monomial>();

        int numberOfElements = Math.max(this.terms.get(0).getPower(), polinom.terms.get(0).getPower()) + 1;

        for(int i = 0; i < numberOfElements; i++) {
            termList.add(i, new Monomial(0, numberOfElements - 1 - i));
        }

        for (Monomial monom : this.terms) {
            int index = numberOfElements - 1 - monom.getPower();
            termList.set(index, termList.get(index).additionOp(monom));
        }

        for (Monomial monom : polinom.terms) {
            int index = numberOfElements - 1 - monom.getPower();
            termList.set(index, termList.get(index).subtractionOp(monom));
        }

        result.terms = termList;
        result.cleaningThePoli();

        return result;
    }

    public Polynomial derivate() {
        this.organize();

        Polynomial result = new Polynomial();

        for (Monomial m : this.terms) {
            result.terms.add(m.derivativeOp());
        }

        result.cleaningThePoli();

        return result;
    }

    public Polynomial integrate() {
        this.organize();

        Polynomial result = new Polynomial();

        for (Monomial m : this.terms) {
            result.terms.add(m.integrationOp());
        }

        result.cleaningThePoli();

        return result;
    }

    public Polynomial multiply(Polynomial polinom) {
        this.organize();
        polinom.organize();

        ArrayList<Monomial> product = new ArrayList<Monomial>();
        Polynomial result = new Polynomial();
        int nbOfElements = polinom.terms.get(0).getPower() + this.terms.get(0).getPower() + 1;

        for (int i = 0; i < nbOfElements; i++) {
            product.add(i, new Monomial(0, nbOfElements - 1 - i));
        }

        for (Monomial monom1 : this.terms) {
            for (Monomial monom2 : polinom.terms) {
                int index = nbOfElements - 1 - (monom1.getPower() + monom2.getPower());
                product.set(index, product.get(index).additionOp(monom1.multiplicationOp(monom2)));
            }
        }

        result.terms = product;
        result.cleaningThePoli();

        return result;
    }

    private ArrayList<Monomial> obtainMonomials(String s) throws InputException {
        ArrayList<Monomial> polinom = new ArrayList<Monomial>();

        s = s.replaceAll("-", "+-");
        String[] string = s.split("\\+");
        String split = "x\\^|x";

        for (String terms : string) {
            if (!terms.equals("")) {
                String[] param = terms.split(split);

                if (param.length == 0) {
                    polinom.add(new Monomial(1, 1));
                } else if (param.length == 1) {

                    if (!param[0].matches("^[0-9]*$||^[0-9.]*$||^-[0-9]*$||^-[0-9.]*$")) {
                        throw new InputException();
                    }

                    if (terms.contains("x")) {
                        if (terms.equals("-x")) {
                            polinom.add(new Monomial(-1, 1));
                        } else {
                            polinom.add(new Monomial(Double.parseDouble(param[0]), 1));
                        }
                    } else {
                        polinom.add(new Monomial(Double.parseDouble(param[0]), 0));
                    }
                } else if (param.length == 2) {

                    if (param[0].equals("")) {

                        if (!param[1].matches("^[0-9]*$||^[0-9.]*$||^-[0-9]*$||^-[0-9.]*$")) {
                            throw new InputException();
                        }
                        polinom.add(new Monomial(1, Integer.parseInt(param[1])));
                    } else {

                        if (!param[0].matches("^[0-9]*$||^[0-9.]*$||^-[0-9]*$||^-[0-9.]*$") || !param[1].matches("^[0-9]*$||^[0-9.]*$||^-[0-9]*$||^-[0-9.]*$")) {
                            throw new InputException();
                        }
                        polinom.add(new Monomial(Double.parseDouble(param[0]), Integer.parseInt(param[1])));
                    }
                }
            }
        }
        return polinom;
    }

    public String toString() {
        String s = "";

        double coefficient;
        int power;

        int polyDegree = this.getDegree();

        if (this.terms.size() == 0) {
            s += "0";
        } else {
            for (Monomial monom : this.terms) {
                coefficient = monom.getCoefficient();
                power = monom.getPower();

                if (coefficient == 0) {
                    s += "0";
                } else {
                    if (power != polyDegree) {
                        s += (coefficient > 0) ? "+" : "";
                    }

                    if (coefficient == -1 && power != 0) {
                        s += "-";
                    }

                    if ((power >= 1 && coefficient != 1 && coefficient != -1) || (power == 0)) {
                        if (Math.floor(coefficient) == coefficient) {
                            s += (int) coefficient;
                        } else {
                            s += coefficient;
                        }
                    }

                    if (power == 1) {
                        s += "x";
                    } else if (power > 1) {
                        s += "x^" + power;
                    }
                }
            }
        }
        return s;
    }

    public ArrayList<Monomial> getElements() {
        return this.terms;
    }

    public int getDegree() {
        return (this.terms.size() == 0) ? 0 : this.terms.get(0).getPower();
    }
}
