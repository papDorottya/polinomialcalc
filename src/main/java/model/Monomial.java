package model;

public class Monomial {
    private double coefficient;
    private int power;

    public Monomial(double coefficient, int power) {
        this.coefficient = coefficient;
        this.power = power;
    }

    public Monomial() {
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Monomial additionOp(Monomial value) {
        Monomial result = new Monomial();

        if(value.getPower() == this.getPower()) {
            result.setCoefficient(this.getCoefficient() + value.getCoefficient());
            result.setPower(value.getPower());

            return result;
        }

        return null;
    }

    public Monomial subtractionOp(Monomial value) {
        Monomial result = new Monomial();

        if(value.getPower() == this.getPower()) {
            result.setCoefficient(this.getCoefficient() - value.getCoefficient());
            result.setPower(result.getPower());

            return result;
        }

        return null;
    }

    public Monomial multiplicationOp(Monomial value) {
        Monomial result = new Monomial();

        result.setCoefficient(value.getCoefficient() * this.getCoefficient());
        result.setPower(result.getPower() + this.getPower());

        return result;
    }

    public Monomial derivativeOp() {
        Monomial result = new Monomial();

        if(this.getPower() == 0) {
            result.setCoefficient(0);
            result.setPower(0);
        } else {
            result.setCoefficient(this.getCoefficient() * this.getPower());
            result.setPower(this.getPower() - 1);
        }

        return result;
    }

    public Monomial integrationOp() {
        Monomial result = new Monomial();

        result.setCoefficient(this.getCoefficient() / (this.getPower() + 1));
        result.setPower(this.getPower() + 1);

        return result;
    }
}
